const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8000;

const PASSWORD = 'javascript';

app.get('/', (req, res) => {
  res.send(`Пройдите по ссылке: 
              encode/... или decode/...
            , что бы проверить шифрование и дешифрование.`);
});

app.get('/encode/:some_word', (req, res) => {
  let encode = Vigenere.Cipher(PASSWORD).crypt(`${req.params.some_word}`);
  res.send(encode);
});

app.get('/decode/:some_word', (req, res) => {
  let decode = Vigenere.Decipher(PASSWORD).crypt(`${req.params.some_word}`);
  res.send(decode);
});

app.get('/:text', (req, res) => {
  res.send(`${req.params.text}`);
});

app.listen(port, () => {
  console.log('We are live on ' + port);
});